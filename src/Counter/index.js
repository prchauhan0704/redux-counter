import React from "react";
import { useSelector, useDispatch } from "react-redux";

export default function Counter() {
  const count = useSelector((state) => state.counter.count);
  const dispatch = useDispatch();

  const increment = () => {
    dispatch({ type: "INCREMENT" });
  };

  const decrement = () => {
    dispatch({ type: "DECREMENT" });
  };

  return (
    <div>
      <div>
        <button onClick={increment}>increment btn</button>
        <span>Counter : {count}</span>
        <button onClick={decrement}>decrement btn</button>
      </div>
    </div>
  );
}
